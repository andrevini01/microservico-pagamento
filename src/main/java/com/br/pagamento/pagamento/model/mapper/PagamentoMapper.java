package com.br.pagamento.pagamento.model.mapper;

import com.br.pagamento.pagamento.client.CartaoClient;
import com.br.pagamento.pagamento.client.CartaoDTO;
import com.br.pagamento.pagamento.model.Pagamento;
import com.br.pagamento.pagamento.model.dto.CreatePagamentoRequest;
import com.br.pagamento.pagamento.model.dto.PagamentoResponse;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    @Autowired
    private CartaoClient cartaoClient;

    public PagamentoResponse toPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        CartaoDTO cartaoDTO = cartaoClient.getById(pagamento.getCartaoId());

        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartaoId(cartaoDTO.getId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());

        return pagamentoResponse;
    }

    public List<PagamentoResponse> toPagamentoResponse(List<Pagamento> pagamentos) {
        List<PagamentoResponse> pagamentoResponses = new ArrayList<>();

        for (Pagamento pagamento : pagamentos) {
            pagamentoResponses.add(toPagamentoResponse(pagamento));
        }

        return pagamentoResponses;
    }

    public static Pagamento toPagamento(CreatePagamentoRequest createPagamentoRequest) {
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setId(createPagamentoRequest.getCartaoId());

        Pagamento pagamento = new Pagamento();

        pagamento.setCartaoId(cartaoDTO.getId());
        pagamento.setDescricao(createPagamentoRequest.getDescricao());
        pagamento.setValor(createPagamentoRequest.getValor());

        return pagamento;
    }
}
