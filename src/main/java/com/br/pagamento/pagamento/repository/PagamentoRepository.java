package com.br.pagamento.pagamento.repository;

import com.br.pagamento.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    List<Pagamento> findAllByCartaoId(Long cartaoId);
}
