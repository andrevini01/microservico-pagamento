package com.br.pagamento.pagamento.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao")
public interface CartaoClient {

    @GetMapping("cartao/buscarporid/{id}")
    CartaoDTO getById(@PathVariable Long id);
}
