package com.br.pagamento.pagamento.controller;

import com.br.pagamento.pagamento.model.Pagamento;
import com.br.pagamento.pagamento.model.dto.CreatePagamentoRequest;
import com.br.pagamento.pagamento.model.dto.PagamentoResponse;
import com.br.pagamento.pagamento.model.mapper.PagamentoMapper;
import com.br.pagamento.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {
    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponse create(@RequestBody CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = PagamentoMapper.toPagamento(createPagamentoRequest);

        pagamento = pagamentoService.create(pagamento);

        return pagamentoMapper.toPagamentoResponse(pagamento);
    }

    @GetMapping("/{cartaoId}")
    public List<PagamentoResponse> listByCartao(@PathVariable Long cartaoId) {
        List<Pagamento> pagamentos = pagamentoService.listByCartao(cartaoId);
        return pagamentoMapper.toPagamentoResponse(pagamentos);
    }
}
