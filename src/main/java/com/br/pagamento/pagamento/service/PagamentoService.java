package com.br.pagamento.pagamento.service;

import com.br.pagamento.pagamento.client.CartaoClient;
import com.br.pagamento.pagamento.client.CartaoDTO;
import com.br.pagamento.pagamento.model.Pagamento;
import com.br.pagamento.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento create(Pagamento pagamento) {
        CartaoDTO cartaoDTO = cartaoClient.getById(pagamento.getCartaoId());

        pagamento.setCartaoId(cartaoDTO.getId());

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> listByCartao(Long cartaoId) {
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }

}
